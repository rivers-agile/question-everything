#!/bin/bash

set -euo pipefail

sudo su - postgres -c "psql -c 'DROP DATABASE IF EXISTS question_everything_test;'"
sudo su - postgres -c "psql -c 'CREATE DATABASE question_everything_test;'"
