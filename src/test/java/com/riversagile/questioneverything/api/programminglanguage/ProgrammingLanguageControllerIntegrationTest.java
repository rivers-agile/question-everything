package com.riversagile.questioneverything.api.programminglanguage;

import com.riversagile.questioneverything.QuestionEverythingApplication;
import com.riversagile.questioneverything.api.common.AbstractDao;
import com.riversagile.questioneverything.api.common.CommonIntegrations;
import com.riversagile.questioneverything.api.common.CommonIntegrationsFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import javax.inject.Inject;
import java.util.UUID;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@SpringBootTest(classes = {QuestionEverythingApplication.class})
@Tag("integration")
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ProgrammingLanguageControllerIntegrationTest {

    @Inject
    private WebApplicationContext context;

    @Inject
    private ProgrammingLanguageDao dao;

    @Inject
    private ModelMapper mapper;

    private MockMvc mvc;

    @BeforeEach
    void setUp() {
        mvc = webAppContextSetup(context).build();
    }

    @Nested
    class ProgrammingLanguageIntegrations
        extends CommonIntegrations<ProgrammingLanguage, ProgrammingLanguageDto> {
        @Override
        protected CommonIntegrationsFactory<ProgrammingLanguage, ProgrammingLanguageDto> getFactory() {
            return new ProgrammingLanguageIntegrationsFactory();
        }
    }

    private class ProgrammingLanguageIntegrationsFactory
        implements CommonIntegrationsFactory<ProgrammingLanguage, ProgrammingLanguageDto> {

        @Override
        public String getPath() {
            return "/api/programming-languages";
        }

        @Override
        public MockMvc getMvc() {
            return mvc;
        }

        @Override
        public AbstractDao<ProgrammingLanguage> getDao() {
            return dao;
        }

        @Override
        public ModelMapper getMapper() {
            return mapper;
        }

        @Override
        public ProgrammingLanguage createEntity() {
            final ProgrammingLanguage entity = new ProgrammingLanguage();
            entity.setName("test programming language " + UUID.randomUUID());
            return dao.create(entity);
        }

        @Override
        public ProgrammingLanguageDto createCreateDto() {
            final ProgrammingLanguageDto dto = new ProgrammingLanguageDto();
            dto.setName("test programming language");
            return dto;
        }

        @Override
        public void updateDto(ProgrammingLanguageDto dto) {
            dto.setName("updated programming language");
        }

        @Override
        public Class<ProgrammingLanguageDto> getDtoClass() {
            return ProgrammingLanguageDto.class;
        }

        @Override
        public Object getUpdatedValueInEntity(ProgrammingLanguage entity) {
            return entity.getName();
        }

        @Override
        public String getDtoUpdatedValueJsonPath() {
            return "$.name";
        }

        @Override
        public Object getUpdatedValue() {
            return "updated programming language";
        }
    }
}
