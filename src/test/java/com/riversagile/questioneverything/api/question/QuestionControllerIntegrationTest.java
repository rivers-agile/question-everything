package com.riversagile.questioneverything.api.question;

import com.riversagile.questioneverything.QuestionEverythingApplication;
import com.riversagile.questioneverything.api.common.AbstractDao;
import com.riversagile.questioneverything.api.common.CommonIntegrations;
import com.riversagile.questioneverything.api.common.CommonIntegrationsFactory;
import com.riversagile.questioneverything.api.programminglanguage.ProgrammingLanguage;
import com.riversagile.questioneverything.api.programminglanguage.ProgrammingLanguageDao;
import com.riversagile.questioneverything.api.quesitoncategory.QuestionCategory;
import com.riversagile.questioneverything.api.quesitoncategory.QuestionCategoryDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import javax.inject.Inject;
import java.util.UUID;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@SpringBootTest(classes = {QuestionEverythingApplication.class})
@Tag("integration")
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class QuestionControllerIntegrationTest {

    @Inject
    private WebApplicationContext context;

    @Inject
    private QuestionDao dao;

    @Inject
    private QuestionCategoryDao categoryDao;

    @Inject
    private ProgrammingLanguageDao languageDao;

    @Inject
    private ModelMapper mapper;

    private MockMvc mvc;

    @BeforeEach
    void setUp() {
        mvc = webAppContextSetup(context).build();
    }

    @Nested
    class QuestionIntegrations extends CommonIntegrations<Question, QuestionDto> {
        @Override
        protected CommonIntegrationsFactory<Question, QuestionDto> getFactory() {
            return new QuestionIntegrationsFactory();
        }
    }

    private class QuestionIntegrationsFactory
        implements CommonIntegrationsFactory<Question, QuestionDto> {

        @Override
        public String getPath() {
            return "/api/questions";
        }

        @Override
        public MockMvc getMvc() {
            return mvc;
        }

        @Override
        public ModelMapper getMapper() {
            return mapper;
        }

        @Override
        public AbstractDao<Question> getDao() {
            return dao;
        }

        @Override
        public Question createEntity() {
            final UUID uuid = UUID.randomUUID();
            final Question entity = new Question();
            ProgrammingLanguage lang = new ProgrammingLanguage();
            QuestionCategory category = new QuestionCategory();

            lang.setName("programming language " + uuid);
            lang = languageDao.create(lang);

            category.setName("question category " + uuid);
            category = categoryDao.create(category);

            entity.setText("test programming language " + uuid);
            entity.setProgrammingLanguage(lang);
            entity.setCategory(category);

            return dao.create(entity);
        }

        @Override
        public QuestionDto createCreateDto() {
            final QuestionDto dto = new QuestionDto();
            dto.setText("test programming language");
            return dto;
        }

        @Override
        public void updateDto(QuestionDto dto) {
            dto.setText("updated text?");
        }

        @Override
        public Class<QuestionDto> getDtoClass() {
            return QuestionDto.class;
        }

        @Override
        public Object getUpdatedValueInEntity(Question entity) {
            return entity.getText();
        }

        @Override
        public String getDtoUpdatedValueJsonPath() {
            return "$.text";
        }

        @Override
        public Object getUpdatedValue() {
            return "updated text?";
        }
    }
}
