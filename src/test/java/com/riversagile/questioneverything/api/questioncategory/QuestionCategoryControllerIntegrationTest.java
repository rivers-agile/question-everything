package com.riversagile.questioneverything.api.questioncategory;

import com.riversagile.questioneverything.QuestionEverythingApplication;
import com.riversagile.questioneverything.api.common.AbstractDao;
import com.riversagile.questioneverything.api.common.CommonIntegrations;
import com.riversagile.questioneverything.api.common.CommonIntegrationsFactory;
import com.riversagile.questioneverything.api.quesitoncategory.QuestionCategory;
import com.riversagile.questioneverything.api.quesitoncategory.QuestionCategoryDao;
import com.riversagile.questioneverything.api.quesitoncategory.QuestionCategoryDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import javax.inject.Inject;
import java.util.UUID;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@SpringBootTest(classes = {QuestionEverythingApplication.class})
@Tag("integration")
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@DisplayName("QuestionCategoryControllerIntegration")
class QuestionCategoryControllerIntegrationTest {

    @Inject
    private WebApplicationContext context;

    @Inject
    private QuestionCategoryDao dao;

    @Inject
    private ModelMapper mapper;

    private MockMvc mvc;

    @BeforeEach
    public void setUp() {
        mvc = webAppContextSetup(context).build();
    }

    @Nested
    class QuestionCategoryIntegrations extends CommonIntegrations<QuestionCategory, QuestionCategoryDto> {
        @Override
        protected CommonIntegrationsFactory<QuestionCategory, QuestionCategoryDto> getFactory() {
            return new QuestionCategoryIntegrationsFactory();
        }
    }

    private class QuestionCategoryIntegrationsFactory
        implements CommonIntegrationsFactory<QuestionCategory, QuestionCategoryDto> {

        @Override
        public String getPath() {
            return "/api/question-categories";
        }

        @Override
        public MockMvc getMvc() {
            return mvc;
        }

        @Override
        public ModelMapper getMapper() {
            return mapper;
        }

        @Override
        public AbstractDao<QuestionCategory> getDao() {
            return dao;
        }

        @Override
        public QuestionCategory createEntity() {
            QuestionCategory c = new QuestionCategory();
            c.setName("test category " + UUID.randomUUID());
            return dao.create(c);
        }

        @Override
        public QuestionCategoryDto createCreateDto() {
            final QuestionCategoryDto dto = new QuestionCategoryDto();
            dto.setName("test question category");
            return dto;
        }

        @Override
        public void updateDto(QuestionCategoryDto dto) {
            dto.setName("updated name");
        }

        @Override
        public Class<QuestionCategoryDto> getDtoClass() {
            return QuestionCategoryDto.class;
        }

        @Override
        public Object getUpdatedValueInEntity(QuestionCategory entity) {
            return entity.getName();
        }

        @Override
        public String getDtoUpdatedValueJsonPath() {
            return "$.name";
        }

        @Override
        public Object getUpdatedValue() {
            return "updated name";
        }
    }
}
