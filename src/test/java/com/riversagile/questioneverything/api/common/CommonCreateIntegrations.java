package com.riversagile.questioneverything.api.common;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static com.riversagile.questioneverything.api.common.TestUtils.toJson;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public abstract class CommonCreateIntegrations<E extends Identifiable, D extends Identifiable> {

    private CommonIntegrationsFactory<E, D> factory;
    private D dto;

    public CommonCreateIntegrations(CommonIntegrationsFactory<E, D> factory) {
        super();
        this.factory = factory;
    }

    @BeforeEach
    void setUp() {
        factory.getDao().deleteAll();
        dto = factory.createCreateDto();
    }

    @AfterEach
    void tearDown() {
        factory.getDao().deleteAll();
    }

    @Test
    @DisplayName("returns a request with the correct headings")
    public void returnsARequestWithTheCorrectHeadings() throws Exception {
        factory.getMvc().perform(post(factory.getPath())
            .content(toJson(dto))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isCreated())
            .andExpect(content().encoding("UTF-8"));
    }

    @Test
    @DisplayName("returns the created object")
    public void returnsTheCreatedObject() throws Exception {
        factory.getMvc().perform(post(factory.getPath())
            .content(toJson(dto))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(jsonPath("$.id").exists());
    }

    @Test
    @DisplayName("creates an object")
    public void createsAnObject() throws Exception {
        factory.getMvc().perform(post(factory.getPath())
            .content(toJson(dto))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andDo(print());
        assertThat(factory.getDao().count(), is(equalTo(1L)));
    }
}
