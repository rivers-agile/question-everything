package com.riversagile.questioneverything.api.common;

import com.riversagile.questioneverything.api.question.Question;
import com.riversagile.questioneverything.api.question.QuestionDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@Tag("unit")
@DisplayName("AbstractController")
@ExtendWith(MockitoExtension.class)
class AbstractControllerTest {

    @InjectMocks
    private TestController ctrl;

    @Mock
    private ModelMapper mapper;

    @Nested
    @DisplayName("toEntity(dto)")
    class ToEntity_Dto {
        private QuestionDto dto;
        private Question entity;

        @BeforeEach
        void setUp() {
            dto = new QuestionDto(UUID.randomUUID());
            entity = new Question(UUID.randomUUID());

            doReturn(entity).when(mapper).map(any(), any());
        }

        @Test
        @DisplayName("converts the DTO to an entity")
        void convertsTheDtoToAnEntity() {
            ctrl.toEntity(dto);
            verify(mapper).map(dto, Question.class);
        }

        @Test
        @DisplayName("returns the entity")
        void returnsTheEntity() {
            assertThat(ctrl.toEntity(dto), is(equalTo(entity)));
        }
    }

    @Nested
    @DisplayName("toEntity(dto, entity)")
    class ToEntity_Dto_Entity {
        private QuestionDto dto;
        private Question entity;

        @BeforeEach
        void setUp() {
            dto = new QuestionDto(UUID.randomUUID());
            entity = new Question(UUID.randomUUID());

            doNothing().when(mapper).map(dto, entity);
        }

        @Test
        @DisplayName("maps the DTO onto the given entity")
        void mapsTheDtoOntoTheGivenEntity() {
            ctrl.toEntity(dto, entity);
            verify(mapper).map(dto, entity);
        }

        @Test
        @DisplayName("returns the entity")
        void returnsTheEntity() {
            assertThat(ctrl.toEntity(dto, entity), is(equalTo(entity)));
        }
    }

    @Nested
    @DisplayName("toDto")
    class ToDto {
        private QuestionDto dto;
        private Question entity;

        @BeforeEach
        void setUp() {
            dto = new QuestionDto(UUID.randomUUID());
            entity = new Question(UUID.randomUUID());

            doReturn(dto).when(mapper).map(any(), any());
        }

        @Test
        @DisplayName("converts the entity to a DTO")
        void convertsTheEntityToADto() {
            ctrl.toDto(entity);
            verify(mapper).map(entity, QuestionDto.class);
        }

        @Test
        @DisplayName("returns the dto")
        void returnsTheDto() {
            assertThat(ctrl.toDto(entity), is(equalTo(dto)));
        }
    }

    @Nested
    @DisplayName("toDtos")
    class ToDtos {
        private List<QuestionDto> dtos;
        private List<Question> entities;

        @BeforeEach
        void setUp() {
            dtos = Arrays.asList(new QuestionDto(UUID.randomUUID()), new QuestionDto(UUID.randomUUID()));
            entities = Arrays.asList(new Question(UUID.randomUUID()), new Question(UUID.randomUUID()));

            doReturn(dtos).when(mapper).map(entities, QuestionDto.LIST_TYPE);
        }

        @Test
        @DisplayName("converts the entities DTOs using the given type")
        void convertsTheEntitiesDtOsUsingTheGivenType() {
            ctrl.toDtos(entities, QuestionDto.LIST_TYPE);
            verify(mapper).map(entities, QuestionDto.LIST_TYPE);
        }

        @Test
        @DisplayName("returns the dto")
        void returnsTheDto() {
            assertThat(ctrl.toDtos(entities, QuestionDto.LIST_TYPE), is(equalTo(dtos)));
        }
    }

    private static final class TestController extends AbstractController<Question, QuestionDto> {
        public TestController() {
            super(Question.class, QuestionDto.class);
        }
    }
}
