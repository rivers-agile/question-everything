package com.riversagile.questioneverything.api.common;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static com.riversagile.questioneverything.api.common.TestUtils.toJson;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public abstract class CommonUpdateIntegrations<E extends Identifiable, D extends Identifiable> {

    private final CommonIntegrationsFactory<E, D> factory;
    private E entity;
    private D dto;
    private ModelMapper mapper;

    public CommonUpdateIntegrations(CommonIntegrationsFactory<E, D> factory) {
        super();
        this.factory = factory;
    }

    @BeforeEach
    void setUp() {
        factory.getDao().deleteAll();
        entity = factory.createEntity();
        mapper = factory.getMapper();

        dto = mapper.map(entity, factory.getDtoClass());
        factory.updateDto(dto);
    }

    @AfterEach
    void tearDown() {
        factory.getDao().deleteAll();
    }

    @Test
    @DisplayName("uses the correct encoding")
    void usesTheCorrectEncoding() throws Exception {
        factory.getMvc().perform(put("{path}/{id}", factory.getPath(), entity.getId())
            .content(toJson(dto))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    @DisplayName("returns HTTP 200 when an object with the given ID can be found")
    void returnsHttp200WhenAnObjectWithTheGivenIdCanBeFound() throws Exception {
        factory.getMvc().perform(put("{path}/{id}", factory.getPath(), entity.getId())
            .content(toJson(dto))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    @DisplayName("returns an HTTP 404 when there is no object with the given ID")
    void returnsAnHttp404WhenThereIsNoObjectWithTheGivenId() throws Exception {
        final UUID id = UUID.randomUUID();
        entity.setId(id);

        factory.getMvc().perform(put("{path}/{id}", factory.getPath(), id)
            .content(toJson(dto))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("returns the updated object")
    void returnsTheUpdatedObject() throws Exception {
        factory.getMvc().perform(put("{path}/{id}", factory.getPath(), entity.getId())
            .content(toJson(dto))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(jsonPath("$.id", is(equalTo(entity.getId().toString()))))
            .andExpect(jsonPath(factory.getDtoUpdatedValueJsonPath(), is(equalTo(factory.getUpdatedValue()))));
    }

    @Test
    @DisplayName("updates the object")
    void updatesTheObject() throws Exception {
        factory.getMvc().perform(put("{path}/{id}", factory.getPath(), entity.getId())
            .content(toJson(dto))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andDo(print());
        assertThat(factory.getUpdatedValueInEntity(factory.getDao().get(entity.getId())),
            is(equalTo(factory.getUpdatedValue())));
    }

    @Test
    @DisplayName("does not update the ID")
    void doesNotUpdateTheId() throws Exception {
        UUID oldId = entity.getId();

        dto.setId(UUID.randomUUID());

        factory.getMvc().perform(put("{path}/{id}", factory.getPath(), oldId)
            .content(toJson(dto))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(jsonPath("$.id", is(equalTo(oldId.toString()))));

    }

    @Test
    @DisplayName("does not update the created date")
    void doesNotUpdateTheCreatedDate() throws Exception {
        final DateAuditable dateEntity = (DateAuditable) entity;
        final DateAuditable dateDto = (DateAuditable) dto;
        final LocalDateTime oldCreatedAt = dateEntity.getCreatedAt();

        dateDto.setCreatedAt(LocalDateTime.of(2019, 1, 15, 0, 0));

        factory.getMvc().perform(put("{path}/{id}", factory.getPath(), entity.getId())
            .content(toJson(dateDto))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(jsonPath("$.createdAt",
                is(equalTo(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(oldCreatedAt)))));
    }

    @Test
    @DisplayName("force updates the updated date")
    void forceUpdatesTheUpdatedDate() throws Exception {
        final DateAuditable dateEntity = (DateAuditable) entity;
        final DateAuditable dateDto = (DateAuditable) dto;
        LocalDateTime oldUpdatedAt = dateEntity.getUpdatedAt();
        LocalDateTime newUpdatedAt = LocalDateTime.of(2019, 1, 15, 0, 0);

        dateDto.setUpdatedAt(newUpdatedAt);

        factory.getMvc().perform(put("{path}/{id}", factory.getPath(), entity.getId())
            .content(toJson(dateDto))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(jsonPath("$.updatedAt",
                is(not(equalTo(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(oldUpdatedAt))))))
            .andExpect(jsonPath("$.updatedAt",
                is(not(equalTo(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(newUpdatedAt))))));
    }
}
