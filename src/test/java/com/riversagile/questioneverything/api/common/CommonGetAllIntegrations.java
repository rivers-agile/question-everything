package com.riversagile.questioneverything.api.common;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public abstract class CommonGetAllIntegrations<E extends Identifiable, D extends Identifiable> {

    private final CommonIntegrationsFactory<E, D> factory;

    public CommonGetAllIntegrations(CommonIntegrationsFactory<E, D> factory) {
        super();
        this.factory = factory;
    }

    @BeforeEach
    void setUp() {
        factory.getDao().deleteAll();
    }

    @AfterEach
    void tearDown() {
        factory.getDao().deleteAll();
    }

    @Test
    @DisplayName("returns a request with the correct headings")
    public void returnsARequestWithTheCorrectHeadings() throws Exception {
        factory.getMvc().perform(get(factory.getPath()))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().encoding("UTF-8"));
    }

    @Test
    @DisplayName("returns an empty array when there are no objects")
    public void returnsAnEmptyArrayWhenThereAreNoObjects() throws Exception {
        factory.getMvc().perform(get(factory.getPath()))
            .andDo(print())
            .andExpect(content().json("[]"));
    }

    @Test
    @DisplayName("returns the objects")
    public void returnsTheObjects() throws Exception {
        final E e1 = factory.createEntity();
        final E e2 = factory.createEntity();

        factory.getMvc().perform(get(factory.getPath()))
            .andDo(print())
            .andExpect(jsonPath("$[0].id", is(equalTo(e1.getId().toString()))))
            .andExpect(jsonPath("$[1].id", is(equalTo(e2.getId().toString()))));
    }
}
