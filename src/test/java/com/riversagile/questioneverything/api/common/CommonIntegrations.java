package com.riversagile.questioneverything.api.common;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;

public abstract class CommonIntegrations<E extends Identifiable, D extends Identifiable> {

    public CommonIntegrations() {
        super();
    }

    protected abstract CommonIntegrationsFactory<E, D> getFactory();

    @Nested
    @DisplayName("getAll")
    class GetAll extends CommonGetAllIntegrations<E, D> {
        public GetAll() {
            super(getFactory());
        }
    }

    @Nested
    @DisplayName("get")
    class Get extends CommonGetIntegrations<E, D> {
        public Get() {
            super(getFactory());
        }
    }

    @Nested
    @DisplayName("create")
    class Create extends CommonCreateIntegrations<E, D> {
        public Create() {
            super(getFactory());
        }
    }

    @Nested
    @DisplayName("update")
    class Update extends CommonUpdateIntegrations<E, D> {
        public Update() {
            super(getFactory());
        }
    }

    @Nested
    @DisplayName("delete")
    class Delete extends CommonDeleteIntegrations<E, D> {
        public Delete() {
            super(getFactory());
        }
    }
}
