package com.riversagile.questioneverything.api.common;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public abstract class CommonDeleteIntegrations<E extends Identifiable, D extends Identifiable> {

    private final CommonIntegrationsFactory<E, D> factory;
    private E entity;

    protected CommonDeleteIntegrations(CommonIntegrationsFactory<E, D> factory) {
        this.factory = factory;
    }

    @BeforeEach
    void setUp() {
        factory.getDao().deleteAll();
        entity = factory.createEntity();
    }

    @AfterEach
    void tearDown() {
        factory.getDao().deleteAll();
    }

    @Test
    @DisplayName("returns HTTP 204")
    void returnsHttp204() throws Exception {
        factory.getMvc().perform(delete("{path}/{id}", factory.getPath(), entity.getId()))
            .andDo(print())
            .andExpect(status().isNoContent());
    }

    @Test
    @DisplayName("deletes the object with the given ID")
    void deletesTheObjectWithTheGivenId() throws Exception {
        factory.getMvc().perform(delete("{path}/{id}", factory.getPath(), entity.getId()))
            .andDo(print());
        assertThrows(NotFoundException.class, () -> factory.getDao().get(entity.getId()));
    }
}
