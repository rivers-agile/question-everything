package com.riversagile.questioneverything.api.common;

import org.modelmapper.ModelMapper;
import org.springframework.test.web.servlet.MockMvc;

public interface CommonIntegrationsFactory<E extends Identifiable, D extends Identifiable> {
    String getPath();

    MockMvc getMvc();

    ModelMapper getMapper();

    AbstractDao<E> getDao();

    E createEntity();

    D createCreateDto();

    void updateDto(D dto);

    Class<D> getDtoClass();

    Object getUpdatedValueInEntity(E entity);

    String getDtoUpdatedValueJsonPath();

    Object getUpdatedValue();
}
