package com.riversagile.questioneverything.api.common;

import com.riversagile.questioneverything.api.question.Question;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Arrays;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@Tag("unit")
@DisplayName("QuestionDao")
@ExtendWith(MockitoExtension.class)
class AbstractDaoTest {

    private TestDao dao;

    @Mock(lenient = true)
    private EntityManager em;

    @BeforeEach
    void setUp() {
        dao = new TestDao();
        dao.em = em;
    }

    @Nested
    @DisplayName("getAll")
    class GetAll {
        @Mock
        private TypedQuery<Question> query;

        @BeforeEach
        void beforeEach() {
            doReturn(query).when(em).createQuery(any(), any());
        }

        @Test
        @DisplayName("creates a query that returns all of the objects")
        void createsAQueryThatReturnsAllOfTheObjects() {
            dao.getAll();
            verify(em).createQuery("SELECT e FROM " + Question.class.getName() + " e", Question.class);
        }

        @Test
        @DisplayName("returns the query results")
        void returnsTheQueryResults() {
            final Question q1 = new Question(UUID.randomUUID());
            final Question q2 = new Question(UUID.randomUUID());

            doReturn(Arrays.asList(q1, q2)).when(query).getResultList();
            assertThat(dao.getAll(), contains(q1, q2));
        }
    }

    @Nested
    @DisplayName("get")
    class Get {
        @Mock
        private Question question;
        private UUID id;

        @BeforeEach
        void beforeEach() {
            id = UUID.randomUUID();
            doReturn(question).when(em).find(any(), any());
        }

        @Test
        @DisplayName("requests the object using the given ID")
        void requestsTheObjectUsingTheGivenId() {
            dao.get(id);
            verify(em).find(Question.class, id);
        }

        @Test
        @DisplayName("throws an exception when there is no object with the given ID")
        void throwsAnExceptionWhenThereIsNoObjectWithTheGivenId() {
            doReturn(null).when(em).find(any(), any());
            assertThrows(NotFoundException.class, () -> dao.get(id));
        }

        @Test
        @DisplayName("returns the object when one is found with the given ID")
        void returnsTheObjectWhenOneIsFoundWithTheGivenId() {
            assertThat(dao.get(id), is(equalTo(question)));
        }
    }

    @Nested
    @DisplayName("create")
    class Create {
        private Question question;
        private TestObjectDao objDao;

        @BeforeEach
        void beforeEach() {
            question = new Question(UUID.randomUUID());

            objDao = new TestObjectDao();
            objDao.em = em;

            doNothing().when(em).persist(any());
        }

        @Test
        @DisplayName("sets the created and updated dates on objects that are date auditable")
        void setsTheCreatedAndUpdatedDatesOnObjectsThatAreDateAuditable() {
            dao.create(question);
            assertThat(question.getCreatedAt(), is(not(nullValue())));
            assertThat(question.getUpdatedAt(), is(not(nullValue())));
        }

        @Test
        @DisplayName("does not attempt to set the created and updated dates on objects that are not date auditable")
        void doesNotAttemptToSetTheCreatedAndUpdatedDatesOnObjectsThatAreNotDateAuditable() {
            assertDoesNotThrow(() -> objDao.create(new Object()));
        }

        @Test
        @DisplayName("persists the question")
        void persistsTheQuestion() {
            dao.create(question);
            verify(em).persist(question);
        }

        @Test
        @DisplayName("returns the question")
        void returnsTheQuestion() {
            assertThat(dao.create(question), is(equalTo(question)));
        }
    }

    @Nested
    @DisplayName("update")
    class Update {
        private Question question;
        private TestObjectDao objDao;

        @BeforeEach
        void beforeEach() {
            question = new Question(UUID.randomUUID());

            objDao = new TestObjectDao();
            objDao.em = em;

            doReturn(question).when(em).merge(any());
        }

        @Test
        @DisplayName("sets the updated date on objects that are date auditable")
        void setsTheUpdatedDateOnObjectsThatAreDateAuditable() {
            dao.update(question);
            assertThat(question.getUpdatedAt(), is(not(nullValue())));
        }

        @Test
        @DisplayName("does not attempt to set the updated date on objects that are not date auditable")
        void doesNotAttemptToSetTheUpdatedDateOnObjectsThatAreNotDateAuditable() {
            assertDoesNotThrow(() -> objDao.update(new Object()));
        }

        @Test
        @DisplayName("merges the question")
        void mergesTheQuestion() {
            dao.update(question);
            verify(em).merge(question);
        }

        @Test
        @DisplayName("returns the question")
        void returnsTheQuestion() {
            assertThat(dao.update(question), is(equalTo(question)));
        }
    }

    @Nested
    @DisplayName("deleteAll")
    class deleteAll {

        @Mock
        private TypedQuery<Question> query;

        private Question q1;
        private Question q2;

        @BeforeEach
        void beforeEach() {
            q1 = new Question(UUID.randomUUID());
            q2 = new Question(UUID.randomUUID());

            doReturn(query).when(em).createQuery(any(), any());
            doReturn(Arrays.asList(q1, q2)).when(query).getResultList();
            doNothing().when(em).remove(any());
        }

        @Test
        @DisplayName("creates a query that returns all of the objects")
        void createsAQueryThatReturnsAllOfTheObjects() {
            dao.deleteAll();
            verify(em).createQuery("SELECT e FROM " + Question.class.getName() + " e", Question.class);
        }

        @Test
        @DisplayName("deletes each of the objects")
        void deletesEachOfTheObjects() {
            dao.deleteAll();
            verify(em).remove(q1);
            verify(em).remove(q2);
        }
    }

    @Nested
    @DisplayName("delete")
    class Delete {
        @Mock
        private Question question;
        private UUID id;

        @BeforeEach
        void beforeEach() {
            id = UUID.randomUUID();
            doReturn(question).when(em).find(any(), any());
            doNothing().when(em).remove(any());
        }

        @Test
        @DisplayName("requests the object using the given ID")
        void requestsTheObjectUsingTheGivenId() {
            dao.delete(id);
            verify(em).find(Question.class, id);
        }

        @Test
        @DisplayName("throws an exception when there is no object with the given ID")
        void throwsAnExceptionWhenThereIsNoObjectWithTheGivenId() {
            doReturn(null).when(em).find(any(), any());
            assertThrows(NotFoundException.class, () -> dao.delete(id));
        }

        @Test
        @DisplayName("deletes the object when one is found with the given ID")
        void deletesTheObjectWhenOneIsFoundWithTheGivenId() {
            dao.delete(id);
            verify(em).remove(question);
        }
    }

    @Nested
    @DisplayName("count")
    class Count {
        @Mock
        private TypedQuery<Long> query;

        @BeforeEach
        void beforeEach() {
            doReturn(query).when(em).createQuery(any(), any());
            doReturn(123L).when(query).getSingleResult();
        }

        @Test
        @DisplayName("creates a query that counts all of the objects")
        void createsAQueryThatCountsAllOfTheObjects() {
            dao.count();
            verify(em).createQuery("SELECT COUNT(e) FROM " + Question.class.getName() + " e", Long.class);
        }

        @Test
        @DisplayName("returns the count")
        void returnsTheCount() {
            assertThat(dao.count(), is(equalTo(123L)));
        }
    }

    private static class TestDao extends AbstractDao<Question> {
        public TestDao() {
            super(Question.class);
        }
    }

    private static class TestObjectDao extends AbstractDao<Object> {
        public TestObjectDao() {
            super(Object.class);
        }
    }
}
