package com.riversagile.questioneverything.api.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public abstract class TestUtils {
    public static String toJson(Object obj) throws Exception {
        return new ObjectMapper()
            .registerModule(new JavaTimeModule())
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
            .writerWithDefaultPrettyPrinter()
            .writeValueAsString(obj);
    }
}
