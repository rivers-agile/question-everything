package com.riversagile.questioneverything.api.common;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public abstract class CommonGetIntegrations<E extends Identifiable, D extends Identifiable> {

    private final CommonIntegrationsFactory<E, D> factory;
    private E entity;

    public CommonGetIntegrations(CommonIntegrationsFactory<E, D> factory) {
        super();
        this.factory = factory;
    }

    @BeforeEach
    void setUp() {
        factory.getDao().deleteAll();
        entity = factory.createEntity();
    }

    @AfterEach
    void tearDown() {
        factory.getDao().deleteAll();
    }

    @Test
    @DisplayName("uses the correct encoding")
    public void usesTheCorrectEncoding() throws Exception {
        factory.getMvc().perform(get("{path}/{id}", factory.getPath(), entity.getId()))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    @DisplayName("returns HTTP 200 when an object with the given ID can be found")
    public void returnsHttp200WhenAnObjectWithTheGivenIdCanBeFound() throws Exception {
        factory.getMvc().perform(get("{path}/{id}", factory.getPath(), entity.getId()))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    @DisplayName("returns an HTTP 404 when there is no object with the given ID")
    public void returnsAnHttp404WhenThereIsNoObjectWithTheGivenId() throws Exception {
        factory.getMvc().perform(get("{path}/{id}", factory.getPath(), UUID.randomUUID()))
            .andDo(print())
            .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("returns the object with the given ID")
    public void returnsTheObjectWithTheGivenId() throws Exception {
        factory.getMvc().perform(get("{path}/{id}", factory.getPath(), entity.getId()))
            .andDo(print())
            .andExpect(jsonPath("$.id", is(equalTo(entity.getId().toString()))));
    }
}
