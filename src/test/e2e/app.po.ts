import {browser, by, element} from 'protractor';

export class AppPage {
    navigateTo() {
        return browser.get('/') as Promise<any>;
    }

    getTitleText() {
        return element(by.css('qe-root h1')).getText() as Promise<string>;
    }
}
