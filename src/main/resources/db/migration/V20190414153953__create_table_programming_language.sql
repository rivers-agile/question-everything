CREATE TABLE programming_language
(
    id         UUID PRIMARY KEY,
    name       VARCHAR UNIQUE,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL
);

ALTER TABLE question
    ADD programming_language_id UUID;
ALTER TABLE question
    ADD FOREIGN KEY (programming_language_id) REFERENCES programming_language (id);
