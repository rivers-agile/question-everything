CREATE TABLE question_category
(
    id         UUID PRIMARY KEY,
    name       VARCHAR UNIQUE,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL
);

ALTER TABLE question
    ADD question_category_id UUID;
ALTER TABLE question
    ADD CONSTRAINT question_category_not_null CHECK (question_category_id IS NOT NULL);
ALTER TABLE question
    ADD FOREIGN KEY (question_category_id) REFERENCES question_category (id);
