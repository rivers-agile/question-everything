CREATE TABLE question_answer
(
    id          UUID PRIMARY KEY,
    question_id UUID NOT NULL,
    text        VARCHAR   NOT NULL,
    created_at  TIMESTAMP NOT NULL,
    updated_at  TIMESTAMP NOT NULL
);

ALTER TABLE question_answer
    ADD FOREIGN KEY (question_id) REFERENCES question (id);
