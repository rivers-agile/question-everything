CREATE TABLE question
(
    id   UUID PRIMARY KEY,
    parent_id UUID,
    text VARCHAR NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL
);

alter table question
    ADD FOREIGN KEY (parent_id) REFERENCES question(id);
