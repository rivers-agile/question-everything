package com.riversagile.questioneverything.api.questionanswer;

import com.riversagile.questioneverything.api.common.DateAuditable;
import com.riversagile.questioneverything.api.common.Identifiable;
import com.riversagile.questioneverything.api.common.MappingConfig;
import lombok.Data;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class QuestionAnswerDto implements Identifiable, DateAuditable {

    private UUID id;
    private String text;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public QuestionAnswerDto() {
        this(null);
    }

    public QuestionAnswerDto(UUID id) {
        super();
        this.id = id;
    }

    @Component
    static class QuestionCategoryDtoMappingConfig implements MappingConfig {

        @Override
        public void configureMapping(ModelMapper mapper) {
            final TypeMap<QuestionAnswerDto, QuestionAnswer> d2e =
                mapper.typeMap(QuestionAnswerDto.class, QuestionAnswer.class);

            d2e.addMappings(m -> m.skip(QuestionAnswer::setId));
            d2e.addMappings(m -> m.skip(QuestionAnswer::setCreatedAt));
            d2e.addMappings(m -> m.skip(QuestionAnswer::setUpdatedAt));
        }
    }
}
