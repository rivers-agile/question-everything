package com.riversagile.questioneverything.api.questionanswer;

import com.riversagile.questioneverything.api.common.DateAuditable;
import com.riversagile.questioneverything.api.common.Identifiable;
import com.riversagile.questioneverything.api.question.Question;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
public class QuestionAnswer implements Identifiable, DateAuditable {

    @Id
    @GeneratedValue
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "question_id", referencedColumnName = "id")
    private Question question;

    @Column(nullable = false)
    private String text;

    @CreatedDate
    private LocalDateTime createdAt;

    @LastModifiedDate
    private LocalDateTime updatedAt;

    public QuestionAnswer() {
        this(null);
    }

    public QuestionAnswer(UUID id) {
        super();
        this.id = id;
    }
}
