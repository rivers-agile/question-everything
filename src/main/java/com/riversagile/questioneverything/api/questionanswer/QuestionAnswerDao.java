package com.riversagile.questioneverything.api.questionanswer;

import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class QuestionAnswerDao {

    @PersistenceContext
    EntityManager em;

    public List<QuestionAnswer> getAll() {
        return em.createQuery("SELECT a FROM " + QuestionAnswer.class.getName() + " a", QuestionAnswer.class)
            .getResultList();
    }

    public QuestionAnswer create(QuestionAnswer ans) {
        em.persist(ans);
        return ans;
    }
}
