package com.riversagile.questioneverything.api.programminglanguage;

import com.riversagile.questioneverything.api.common.AbstractController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.UUID;

@Slf4j
@BasePathAwareController
@RequestMapping("programming-languages")
public class ProgrammingLanguageController extends AbstractController<ProgrammingLanguage, ProgrammingLanguageDto> {

    private final ProgrammingLanguageDao dao;

    @Inject
    public ProgrammingLanguageController(ProgrammingLanguageDao dao) {
        super(ProgrammingLanguage.class, ProgrammingLanguageDto.class);
        this.dao = dao;
    }

    @GetMapping
    @ResponseBody
    public Collection<ProgrammingLanguageDto> getAll() {
        log.info("getting all programming languages");
        return toDtos(dao.getAll(), ProgrammingLanguageDto.LIST_TYPE);
    }

    @GetMapping("{id}")
    @ResponseBody
    public ProgrammingLanguageDto get(@PathVariable("id") UUID id) {
        log.info("getting programming language by ID: {}", id);
        return toDto(dao.get(id));
    }

    @PostMapping
    @ResponseBody
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public ProgrammingLanguageDto create(@RequestBody ProgrammingLanguageDto dto) {
        log.info("creating programming language: {}", dto);
        return toDto(dao.create(toEntity(dto)));
    }

    @PutMapping("{id}")
    @ResponseBody
    @Transactional
    public ProgrammingLanguageDto update(@PathVariable("id") UUID id, @RequestBody ProgrammingLanguageDto dto) {
        log.info("updating programming language: id={}, language={}", id, dto);
        final ProgrammingLanguage entity = dao.get(id);
        return toDto(dao.update(toEntity(dto, entity)));
    }

    @DeleteMapping("{id}")
    @Transactional
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") UUID id) {
        log.info("deleting programming language: {}", id);
        dao.delete(id);
    }
}
