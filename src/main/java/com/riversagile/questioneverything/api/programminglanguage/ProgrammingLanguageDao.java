package com.riversagile.questioneverything.api.programminglanguage;

import com.riversagile.questioneverything.api.common.AbstractDao;
import org.springframework.stereotype.Component;

@Component
public class ProgrammingLanguageDao extends AbstractDao<ProgrammingLanguage> {

    public ProgrammingLanguageDao() {
        super(ProgrammingLanguage.class);
    }
}
