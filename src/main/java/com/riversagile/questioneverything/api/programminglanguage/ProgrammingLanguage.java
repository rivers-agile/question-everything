package com.riversagile.questioneverything.api.programminglanguage;

import com.riversagile.questioneverything.api.common.Lookup;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
public class ProgrammingLanguage implements Lookup {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(unique = true, nullable = false)
    private String name;

    @CreatedDate
    @Column(nullable = false)
    private LocalDateTime createdAt;

    @LastModifiedDate
    @Column(nullable = false)
    private LocalDateTime updatedAt;

    public ProgrammingLanguage() {
        this(null);
    }

    public ProgrammingLanguage(UUID id) {
        super();
        this.id = id;
    }
}
