package com.riversagile.questioneverything.api.quesitoncategory;

import com.riversagile.questioneverything.api.common.AbstractDao;
import org.springframework.stereotype.Component;

@Component
public class QuestionCategoryDao extends AbstractDao<QuestionCategory> {

    public QuestionCategoryDao() {
        super(QuestionCategory.class);
    }
}
