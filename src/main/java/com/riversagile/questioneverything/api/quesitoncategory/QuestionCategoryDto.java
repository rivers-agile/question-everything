package com.riversagile.questioneverything.api.quesitoncategory;

import com.riversagile.questioneverything.api.common.Lookup;
import com.riversagile.questioneverything.api.common.MappingConfig;
import com.riversagile.questioneverything.api.question.QuestionDto;
import lombok.Data;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
public class QuestionCategoryDto implements Lookup {

    public static final Type LIST_TYPE = new TypeToken<List<QuestionDto>>() {}.getType();

    private UUID id;
    private String name;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public QuestionCategoryDto() {
        this(null);
    }

    public QuestionCategoryDto(UUID id) {
        super();
        this.id = id;
    }

    @Component
    static class QuestionCategoryDtoMappingConfig implements MappingConfig {

        @Override
        public void configureMapping(ModelMapper mapper) {
            final TypeMap<QuestionCategoryDto, QuestionCategory> d2e =
                mapper.typeMap(QuestionCategoryDto.class, QuestionCategory.class);

            d2e.addMappings(m -> m.skip(QuestionCategory::setId));
            d2e.addMappings(m -> m.skip(QuestionCategory::setCreatedAt));
            d2e.addMappings(m -> m.skip(QuestionCategory::setUpdatedAt));
        }
    }
}
