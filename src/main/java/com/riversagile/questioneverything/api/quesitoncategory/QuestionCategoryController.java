package com.riversagile.questioneverything.api.quesitoncategory;

import com.riversagile.questioneverything.api.common.AbstractController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.UUID;

@Slf4j
@BasePathAwareController
@RequestMapping("question-categories")
public class QuestionCategoryController extends AbstractController<QuestionCategory, QuestionCategoryDto> {

    private final QuestionCategoryDao qcDao;

    @Inject
    public QuestionCategoryController(QuestionCategoryDao qcDao) {
        super(QuestionCategory.class, QuestionCategoryDto.class);
        this.qcDao = qcDao;
    }

    @GetMapping
    @ResponseBody
    public Collection<QuestionCategoryDto> getAll() {
        log.info("getting all question categories");
        return toDtos(qcDao.getAll(), QuestionCategoryDto.LIST_TYPE);
    }

    @GetMapping("{id}")
    @ResponseBody
    public QuestionCategoryDto get(@PathVariable("id") UUID id) {
        log.info("getting question category by ID: {}", id);
        return toDto(qcDao.get(id));
    }

    @PostMapping
    @ResponseBody
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public QuestionCategoryDto create(@RequestBody QuestionCategoryDto dto) {
        log.info("creating question category: {}", dto);
        return toDto(qcDao.create(toEntity(dto)));
    }

    @PutMapping("{id}")
    @ResponseBody
    @Transactional
    public QuestionCategoryDto update(@PathVariable("id") UUID id, @RequestBody QuestionCategoryDto dto) {
        log.info("updating question category: id={}, category={}", id, dto);
        final QuestionCategory entity = qcDao.get(id);
        return toDto(qcDao.update(toEntity(dto, entity)));
    }

    @DeleteMapping("{id}")
    @Transactional
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") UUID id) {
        log.info("deleting question category: {}", id);
        qcDao.delete(id);
    }
}
