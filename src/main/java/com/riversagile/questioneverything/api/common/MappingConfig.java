package com.riversagile.questioneverything.api.common;

import org.modelmapper.ModelMapper;

public interface MappingConfig {
    void configureMapping(final ModelMapper mapper);
}
