package com.riversagile.questioneverything.api.common;

import org.modelmapper.ModelMapper;

import javax.inject.Inject;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

public abstract class AbstractController<E, D> {

    @Inject
    ModelMapper mapper;

    private Class<E> entityClass;
    private Class<D> dtoClass;

    public AbstractController(Class<E> entityClass, Class<D> dtoClass) {
        this.entityClass = entityClass;
        this.dtoClass = dtoClass;
    }

    protected E toEntity(D dto) {
        return mapper.map(dto, entityClass);
    }

    protected E toEntity(D dto, E entity) {
        mapper.map(dto, entity);
        return entity;
    }

    protected D toDto(E entity) {
        return mapper.map(entity, dtoClass);
    }

    protected List<D> toDtos(Collection<E> entities, Type type) {
        return mapper.map(entities, type);
    }
}
