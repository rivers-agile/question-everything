package com.riversagile.questioneverything.api.common;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {
    public NotFoundException(UUID id, Class<?> clazz) {
        this(id, clazz, null);
    }

    public NotFoundException(UUID id, Class<?> clazz, Throwable cause) {
        super("could not find entity with id: id=" + id + ", class=" + clazz.getName(), cause);
    }
}
