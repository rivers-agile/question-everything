package com.riversagile.questioneverything.api.common;

public interface Lookup extends Identifiable, DateAuditable {
    String getName();

    void setName(String name);
}
