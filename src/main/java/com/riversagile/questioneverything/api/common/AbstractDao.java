package com.riversagile.questioneverything.api.common;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public abstract class AbstractDao<E> {

    private final Class<E> clazz;

    @PersistenceContext
    EntityManager em;

    public AbstractDao(@NotNull Class<E> clazz) {
        super();
        this.clazz = clazz;
    }

    public List<E> getAll() {
        return em.createQuery("SELECT e FROM " + clazz.getName() + " e", clazz).getResultList();
    }

    public E get(UUID id) {
        final E entity = em.find(clazz, id);

        if(entity == null) {
            throw new NotFoundException(id, clazz);
        }

        return entity;
    }

    @Transactional
    public E create(E entity) {
        if(entity instanceof DateAuditable) {
            ((DateAuditable) entity).setCreatedAt(LocalDateTime.now());
            ((DateAuditable) entity).setUpdatedAt(LocalDateTime.now());
        }

        em.persist(entity);
        return entity;
    }

    @Transactional
    public E update(E entity) {
        if(entity instanceof DateAuditable) {
            ((DateAuditable) entity).setUpdatedAt(LocalDateTime.now());
        }

        return em.merge(entity);
    }

    @Transactional
    public void deleteAll() {
        em.createQuery("SELECT e FROM " + clazz.getName() + " e", clazz)
            .getResultList()
            .forEach((entity) -> em.remove(entity));
    }

    @Transactional
    public void delete(UUID id) {
        final E entity = em.find(clazz, id);

        if(entity == null) {
            throw new NotFoundException(id, clazz);
        }

        em.remove(entity);
    }

    public long count() {
        return em.createQuery("SELECT COUNT(e) FROM " + clazz.getName() + " e", Long.class).getSingleResult();
    }
}
