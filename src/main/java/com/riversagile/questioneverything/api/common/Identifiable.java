package com.riversagile.questioneverything.api.common;

import java.util.UUID;

public interface Identifiable {
    UUID getId();

    void setId(UUID id);
}
