package com.riversagile.questioneverything.api.common;

import java.time.LocalDateTime;

public interface DateAuditable {
    LocalDateTime getCreatedAt();

    void setCreatedAt(LocalDateTime date);

    LocalDateTime getUpdatedAt();

    void setUpdatedAt(LocalDateTime date);
}
