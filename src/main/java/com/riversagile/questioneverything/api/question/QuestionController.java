package com.riversagile.questioneverything.api.question;

import com.riversagile.questioneverything.api.common.AbstractController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.UUID;

@Slf4j
@BasePathAwareController
@RequestMapping("questions")
public class QuestionController extends AbstractController<Question, QuestionDto> {

    private final QuestionDao dao;

    @Inject
    public QuestionController(QuestionDao dao) {
        super(Question.class, QuestionDto.class);
        this.dao = dao;
    }

    @GetMapping
    @ResponseBody
    @Transactional
    public Collection<QuestionDto> getAll() {
        log.info("getting all questions");
        return toDtos(dao.getAll(), QuestionDto.LIST_TYPE);
    }

    @GetMapping("{id}")
    @ResponseBody
    @Transactional
    public QuestionDto get(@PathVariable("id") UUID id) {
        log.info("getting question by ID: {}", id);
        return toDto(dao.get(id));
    }

    @PostMapping
    @ResponseBody
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public QuestionDto create(@RequestBody QuestionDto dto) {
        log.info("creating question: {}", dto);
        return toDto(dao.create(toEntity(dto)));
    }

    @PutMapping("{id}")
    @ResponseBody
    @Transactional
    public QuestionDto update(@PathVariable("id") UUID id, @RequestBody QuestionDto dto) {
        log.info("updating question: id={}, question={}", id, dto);
        final Question entity = dao.get(id);
        return toDto(dao.update(toEntity(dto, entity)));
    }

    @DeleteMapping("{id}")
    @Transactional
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") UUID id) {
        log.info("deleting question: {}", id);
        dao.delete(id);
    }
}
