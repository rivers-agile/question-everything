package com.riversagile.questioneverything.api.question;

import com.riversagile.questioneverything.api.common.AbstractDao;
import org.springframework.stereotype.Component;

@Component
public class QuestionDao extends AbstractDao<Question> {

    public QuestionDao() {
        super(Question.class);
    }
}
