package com.riversagile.questioneverything.api.question;

import com.riversagile.questioneverything.api.common.DateAuditable;
import com.riversagile.questioneverything.api.common.Identifiable;
import com.riversagile.questioneverything.api.common.MappingConfig;
import com.riversagile.questioneverything.api.programminglanguage.ProgrammingLanguageDto;
import com.riversagile.questioneverything.api.quesitoncategory.QuestionCategoryDto;
import lombok.Data;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
public class QuestionDto implements Identifiable, DateAuditable {

    public static final Type LIST_TYPE = new TypeToken<List<QuestionDto>>() {}.getType();

    private UUID id;
    private String text;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private UUID parentId;
    private ProgrammingLanguageDto programmingLanguage;
    private QuestionCategoryDto category;

    public QuestionDto() {
        this(null);
    }

    public QuestionDto(UUID id) {
        super();
        this.id = id;
    }

    @Component
    static class QuestionDtoMappingConfig implements MappingConfig {

        @Override
        public void configureMapping(final ModelMapper mapper) {
            final TypeMap<QuestionDto, Question> d2e = mapper.typeMap(QuestionDto.class, Question.class);

            d2e.addMappings(m -> m.skip(Question::setId));
            d2e.addMappings(m -> m.skip(Question::setCreatedAt));
            d2e.addMappings(m -> m.skip(Question::setUpdatedAt));
            d2e.addMappings(m -> m.skip(Question::setUpdatedAt));
        }
    }
}
