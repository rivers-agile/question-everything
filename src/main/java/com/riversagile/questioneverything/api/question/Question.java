package com.riversagile.questioneverything.api.question;

import com.riversagile.questioneverything.api.common.DateAuditable;
import com.riversagile.questioneverything.api.common.Identifiable;
import com.riversagile.questioneverything.api.programminglanguage.ProgrammingLanguage;
import com.riversagile.questioneverything.api.quesitoncategory.QuestionCategory;
import com.riversagile.questioneverything.api.questionanswer.QuestionAnswer;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Data
@Entity
public class Question implements Identifiable, DateAuditable {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(unique = true)
    private String text;

    @CreatedDate
    @Column(nullable = false)
    private LocalDateTime createdAt;

    @LastModifiedDate
    @Column(nullable = false)
    private LocalDateTime updatedAt;

    @ManyToOne
    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    private Question parent;

    @ManyToOne
    @JoinColumn(name = "programming_language_id", referencedColumnName = "id")
    private ProgrammingLanguage programmingLanguage;

    @ManyToOne
    @JoinColumn(name = "question_category_id", referencedColumnName = "id")
    private QuestionCategory category;

    @ToString.Exclude
    @OneToMany(mappedBy = "question")
    private Set<QuestionAnswer> answers;

    public Question() {
        this(null);
    }

    public Question(UUID id) {
        super();
        this.id = id;
    }
}
