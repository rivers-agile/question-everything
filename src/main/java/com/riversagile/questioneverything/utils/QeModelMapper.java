package com.riversagile.questioneverything.utils;

import com.riversagile.questioneverything.api.common.MappingConfig;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

@Component
public class QeModelMapper extends ModelMapper {
    private final List<MappingConfig> dtoMappers;

    @Inject
    public QeModelMapper(List<MappingConfig> dtoMappers) {
        super();
        this.dtoMappers = dtoMappers;
        this.configureMappings();
    }

    void configureMappings() {
        this.dtoMappers.forEach((m) -> m.configureMapping(this));
    }
}
