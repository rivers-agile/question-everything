package com.riversagile.questioneverything.utils;

import com.riversagile.questioneverything.api.common.Identifiable;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

public class IdSet<I extends Identifiable> extends AbstractSet<I> {

    private final HashMap<UUID, I> map;

    /**
     * Creates an empty {@link IdSet}.
     */
    public IdSet() {
        this((Iterator<I>) null);
    }

    /**
     * Creates an {@link IdSet} that will initially contain the values from the given {@link Iterable}.
     *
     * @param it The <code>Iterable</code> that contains the values that will be added to the set. If <code>null</code>,
     *           the set will be empty.
     * @see #addAll(Iterator)
     */
    public IdSet(Iterable<I> it) {
        this(it == null ? null : it.iterator());
    }

    /**
     * Creates an {@link IdSet} that will initially contain the values from the given {@link Iterator}.
     *
     * @param it The <code>Iterator</code> that contains the values that will be added to the set. If <code>null</code>,
     *           the set will be empty.
     * @see #addAll(Iterator)
     */
    public IdSet(Iterator<I> it) {
        super();

        map = new HashMap<>();
        addAll(it);
    }

    /**
     * Adds the given {@link Identifiable} to the set. If an <code>Identifiable</code> with the same ID is already
     * contained within the set, the already existing <code>Identifiable</code> will be overwritten.
     *
     * @param i The element to be added to the set. If <code>null</code> or has a <code>null</code> ID, the element will
     *          not be added and <code>false</code> will be returned.
     * @return Returns <code>true</code> if the element was added to the set. Returns <code>false</code> otherwise.
     * @see Identifiable#getId()
     */
    @Override
    public boolean add(I i) {
        boolean added = false;

        if(i != null) {
            final UUID id = i.getId();

            if(id != null) {
                map.put(id, i);
                added = true;
            }
        }

        return added;
    }

    /**
     * Adds the elements from the given {@link Iterator} to the set.
     *
     * @param it The iterator that contains values that should be added to the set.
     * @return Returns <code>true</code> if the set changed as a result of this call. Returns <code>false</code>
     * otherwise.
     * @see #addAll(Collection)
     * @see #add(Identifiable)
     */
    public boolean addAll(Iterator<I> it) {
        boolean changed = false;

        if(it != null) {
            while(it.hasNext()) {
                if(add(it.next())) {
                    changed = true;
                }
            }
        }

        return changed;
    }

    /**
     * Returns the {@link Identifiable} contained within the set whose ID is equal to the given
     * <code>Identifiable</code>'s ID.
     *
     * @param i The <code>Identifiable</code> whose ID will be used to find a set element. If <code>null</code>,
     *          <code>null</code> will always be returned.
     * @return Returns the element in the set that matches the given <code>Identifiable</code>'s ID. If no element can
     * be found, <code>null</code> is returned.
     * @see #get(UUID)
     * @see Identifiable#getId()
     */
    public I get(I i) {
        return i == null ? null : get(i.getId());
    }

    /**
     * Returns the {@link Identifiable} contained within the set whose ID is equal to the given {@link UUID}.
     *
     * @param id The ID of the <code>Identifiable</code> to be retrieved. If <code>null</code>, <code>null</code> will
     *           always be returned.
     * @return Returns the element in the set that whose ID is equal to the given <code>UUID</code>. If no element
     * can be found, <code>null</code> is returned.
     * @see #get(Identifiable)
     * @see Identifiable#getId()
     */
    public I get(UUID id) {
        return id == null ? null : map.get(id);
    }

    /**
     * Removes the given object from the set. An object will only be removed from the set if it is one of the following
     * types:
     * <ul>
     * <li>{@link UUID} - The {@link Identifiable} within the set whose ID is equal to the given <code>UUID</code> will
     * be removed.</li>
     * <li>{@link Identifiable} - The <code>Identifiable</code> within the set whose ID equals this
     * <code>Identifiable</code>'s ID will be removed.</li>
     * </ul>
     *
     * @param o The object to be removed. If <code>null</code>, <code>false</code> will always be returned.
     * @return Returns <code>true</code> if an element was removed. Returns <code>false</code> otherwise.
     * @see #remove(Identifiable)
     * @see #remove(UUID)
     * @see Identifiable#getId()
     * @deprecated Deprecated because this function allows you to remove elements of types that are different from the
     * type specified by the class.
     */
    @Override
    @Deprecated
    public boolean remove(Object o) {
        boolean removed = false;

        if(o != null) {
            if(o instanceof UUID) {
                removed = remove(((UUID) o)) != null;
            } else if(o instanceof Identifiable) {
                removed = remove(((Identifiable) o).getId()) != null;
            }
        }

        return removed;
    }

    /**
     * Removes the {@link Identifiable} contained within the set whose ID is equal to the given
     * <code>Identifiable</code>'s ID.
     *
     * @param i The <code>Identifiable</code> whose ID will be used to remove a set element. If <code>null</code>,
     *          <code>false</code> will always be returned.
     * @return If an <code>Identifiable</code> is removed, <code>true</code> is returned. If nothing is removed,
     * <code>false</code> is returned.
     * @see #remove(UUID)
     * @see Identifiable#getId()
     */
    public boolean remove(I i) {
        boolean removed = false;

        if(i != null) {
            removed = remove(i.getId()) != null;
        }

        return removed;
    }

    /**
     * Removes the {@link Identifiable} contained within the set whose ID is equal to the given {@link UUID}.
     *
     * @param id The ID of the <code>Identifiable</code> to be removed. If <code>null</code>, <code>false</code> will
     *           always be returned.
     * @return If an <code>Identifiable</code> matching the given ID is removed, that <code>Identifiable</code> is
     * returned. If nothing is removed, <code>false</code> is returned.
     * @see #remove(Identifiable)
     * @see Identifiable#getId()
     */
    public I remove(UUID id) {
        I removed = null;

        if(id != null) {
            removed = map.remove(id);
        }

        return removed;
    }

    /**
     * Returns whether or not the given object is within the set. Objects of the following types will be considered to
     * be contained within the set:
     * <ul>
     * <li>{@link UUID} - The {@link Identifiable} within the set whose ID equals the <code>UUID</code> will be
     * checked for.</li>
     * <li>{@link Identifiable} - The <code>Identifiable</code> within the set whose ID equals this
     * <code>Identifiable</code>'s ID will be checked for.</li>
     * </ul>
     *
     * @param o The object to be checked for. If <code>null</code>, <code>false</code> will always be returned.
     * @return Returns <code>true</code> if the object is within the set. Returns <code>false</code> otherwise.
     * @see #remove(Identifiable)
     * @see #remove(UUID)
     * @see Identifiable#getId()
     * @deprecated Deprecated because this function will consider elements of different types to be within the set.
     */
    @Override
    @Deprecated
    public boolean contains(Object o) {
        boolean contains = false;

        if(o != null) {
            if(o instanceof Number) {
                contains = contains((UUID) o);
            } else if(o instanceof Identifiable) {
                contains = contains(((Identifiable) o).getId());
            }
        }

        return contains;
    }

    /**
     * Returns whether or not the set contains an {@link Identifiable} whose ID is equal to the ID of the given
     * <code>Identifiable</code>.
     *
     * @param i The <code>Identifiable</code> that is to be checked for. If <code>null</code>, <code>false</code> will
     *          always be returned.
     * @return Returns <code>true</code> if the set contains an <code>Identifiable</code> whose ID is equal to the ID of
     * the given <code>Identifiable</code>. If the given <code>Identifiable</code> is <code>null</code>,
     * <code>false</code> will always be returned.
     * @see #contains(UUID)
     * @see Identifiable#getId()
     */
    public boolean contains(I i) {
        return i != null && contains(i.getId());
    }

    /**
     * Returns whether or not the set contains an {@link Identifiable} whose ID equals the given {@link UUID}.
     *
     * @param id The ID of the <code>Identifiable</code> that is to be checked for. If <code>null</code>,
     *           <code>false</code> will always be returned.
     * @return Returns <code>true</code> if the set contains an <code>Identifiable</code> whose ID is equal to the given
     * <code>Integer</code>. Returns <code>false</code> otherwise. If the given ID is <code>null</code>,
     * <code>false</code> will always be returned.
     * @see #contains(Identifiable)
     * @see Identifiable#getId()
     */
    public boolean contains(UUID id) {
        return id != null && map.containsKey(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<I> iterator() {
        return map.values().iterator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return map.size();
    }

    /**
     * Removes all elements from the set.
     */
    @Override
    public void clear() {
        map.clear();
    }
}
