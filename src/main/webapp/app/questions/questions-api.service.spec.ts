import {HttpClient} from '@angular/common/http';
import {Observable, Observer} from 'rxjs';
import {Question} from '../types/question';
import {QuestionsApiService} from './questions-api.service';

describe(QuestionsApiService.name, () => {
    let srv: QuestionsApiService;
    let http: HttpClient;

    beforeEach(() => {
        http = <HttpClient>{};
        (<any>http).get = () => {};

        srv = new QuestionsApiService(http);
    });

    describe('getAll', () => {
        let getObserver: Observer<Question[]>;

        beforeEach(() => {
            spyOn(http, 'get').and.returnValue(Observable.create((observer) => getObserver = observer));
        });

        it('gets the questions', () => {
            srv.getAll();
            expect(http.get).toHaveBeenCalledWith('/api/questions');
        });

        it('returns the questions observable', (done) => {
            srv.getAll().subscribe((questions) => {
                expect(questions).toEqual([{id: 'a'}, {id: 'b'}]);
                done();
            });
            getObserver.next([{id: 'a'}, {id: 'b'}]);
        });
    });
});
