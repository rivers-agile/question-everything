import {Observable, Observer} from 'rxjs';
import {Question} from '../types/question';
import {QuestionsApiService} from './questions-api.service';
import {QuestionsService} from './questions.service';
import Spy = jasmine.Spy;

describe(QuestionsService.name, () => {
    let srv: QuestionsService;
    let questionsApiSrv: QuestionsApiService;

    beforeEach(() => {
        questionsApiSrv = <QuestionsApiService>{
            getAll() { return Observable.create(); },
        };
        srv = new QuestionsService(questionsApiSrv);
    });

    describe('_loadQuestions', () => {
        let getAllObserver: Observer<Question[]>;

        beforeEach(() => {
            spyOn(srv.questionsLoading$, 'next').and.stub();
            spyOn(srv.questions$, 'next').and.stub();
            spyOn(questionsApiSrv, 'getAll').and
                .returnValue(Observable.create((observer) => getAllObserver = observer));
        });

        afterEach(() => {
            getAllObserver.complete();
        });

        it('gets all of the questions', () => {
            srv._loadQuestions();
            expect(questionsApiSrv.getAll).toHaveBeenCalled();
        });

        it('fires the loaded questions when the load is successful', () => {
            srv._loadQuestions();
            getAllObserver.next([{id: 'a'}, {id: 'b'}]);
            expect(srv.questions$.next).toHaveBeenCalledWith([{id: 'a'}, {id: 'b'}]);
        });

        it('does not fire any questions when the load fails', () => {
            srv._loadQuestions();
            getAllObserver.error({});
            expect(srv.questions$.next).not.toHaveBeenCalled();
        });

        it('loading flag event with true when the load starts', () => {
            srv._loadQuestions();
            expect(srv.questionsLoading$.next).toHaveBeenCalledWith(true);
        });

        it('loading flag event with false when the load is successful', () => {
            srv._loadQuestions();
            getAllObserver.next([{id: 'a'}, {id: 'b'}]);
            expect((<Spy>srv.questionsLoading$.next).calls.allArgs()).toEqual([[true], [false]]);
        });

        it('loading flag event with false when the load fails', () => {
            srv._loadQuestions();
            getAllObserver.error({});
            expect((<Spy>srv.questionsLoading$.next).calls.allArgs()).toEqual([[true], [false]]);
        });
    });
});
