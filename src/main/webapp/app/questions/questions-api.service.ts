import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Question} from '../types/question';

@Injectable({providedIn: 'root'})
export class QuestionsApiService {
    constructor(private http: HttpClient) {
    }

    getAll(): Observable<Question[]> {
        return this.http.get<Question[]>('/api/questions');
    }
}
