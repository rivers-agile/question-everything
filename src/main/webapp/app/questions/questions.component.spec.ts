import {BehaviorSubject} from 'rxjs';
import {Question} from '../types/question';
import {QuestionsComponent} from './questions.component';
import {QuestionsService} from './questions.service';

describe(QuestionsComponent.name, () => {
    let ctrl: QuestionsComponent;
    let questionsSrv: QuestionsService;

    beforeEach(() => {
        questionsSrv = <QuestionsService>{};
        ctrl = new QuestionsComponent(questionsSrv);
    });

    describe('ngOnInit', () => {
        let questions$: BehaviorSubject<Question[]>;

        beforeEach(() => {
            questions$ = new BehaviorSubject([]);
            questionsSrv.questions$ = questions$;
        });

        afterEach(() => {
            questions$.complete();
        });

        it('sets the questions when they change', () => {
            ctrl.ngOnInit();
            questions$.next([{id: 'a'}, {id: 'b'}]);
            expect(ctrl.questions).toEqual([{id: 'a'}, {id: 'b'}]);
        });
    });
});
