import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Question} from '../types/question';
import {QuestionsApiService} from './questions-api.service';

@Injectable({providedIn: 'root'})
export class QuestionsService {
    questionsLoading$: BehaviorSubject<boolean> = new BehaviorSubject(false);
    questions$: BehaviorSubject<Question[]> = new BehaviorSubject([]);

    constructor(private questionsApi: QuestionsApiService) {
        this._loadQuestions();
    }

    _loadQuestions() {
        this.questionsLoading$.next(true);
        const obs = this.questionsApi.getAll();

        obs.subscribe(
            (questions) => {
                this.questions$.next(questions);
                this.questionsLoading$.next(false);
                return questions;
            },
            (err) => {
                this.questionsLoading$.next(false);
                console.log('error loading questions', err);
                return err;
            },
        );

        return obs;
    }
}

