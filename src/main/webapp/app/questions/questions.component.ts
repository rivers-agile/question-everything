import {Component, OnInit} from '@angular/core';
import {Question} from '../types/question';
import {QuestionsService} from './questions.service';

@Component({
    selector: 'qe-questions',
    templateUrl: './questions.component.html',
    styleUrls: ['./questions.component.css'],
})
export class QuestionsComponent implements OnInit {
    questions: Question[];

    constructor(private questionsSrv: QuestionsService) {
    }

    ngOnInit(): void {
        this.questionsSrv.questions$.subscribe((questions) => this.questions = questions);
    }
}
