import {Component} from '@angular/core';

@Component({
    selector: 'qe-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'question-everything';
}
