# Question Everything

Manage your questions about everything.

## Setup Instructions

### Install Manual Dependencies

The following dependencies need to be manually installed:

*  Firefox (latest)
*  Java v11
*  Node v8
*  NPM v3
*  Postgres v10

#### Firefox

Firefox is required to test the client-side code.  For Firefox installation
instructions, see one of these websites:

*  Linux - https://support.mozilla.org/en-US/kb/install-firefox-linux
*  Windows - https://support.mozilla.org/en-US/kb/how-download-and-install-firefox-windows
*  MacOS - https://support.mozilla.org/en-US/kb/how-download-and-install-firefox-mac

#### Java v11

Java is required to run the server-side code.  For Java v11 installation
instructions, see this website:

https://openjdk.java.net/install/

### Node v8

Node is required to build and run the client-side code.  For Node v8
installation, see this website: 

https://nodejs.org/en/download/package-manager/

### NPM v3

NPM is required to install the client-side dependencies.  For NPM v3
installation instructions, see this website:

https://www.npmjs.com/get-npm

#### Postgres v10

Postgres is the database that is used when developing.  For GNU/Linux, the
Postgres v10 installation instructions are as follows:
    
```sh
sudo apt install postgresql-10                     # (1)
sudo su -u postgres                                # (2)
passwd                                             # (3)
psql                                               # (4)
\password                                          # (5)
CREATE DATABASE question_everything ENCODING utf8; # (6)
```

1.  Install Postgres v10 using the `apt` package manager
2.  When Postgres is installed, it will automatically create a user named
    `postgres`.  This will log you in as this user.
3.  Set the password on the `postgres` GNU/Linux user
4.  Open the Postgres interactive terminal
5.  Set the password on the `postgres` database user
6.  Create the `question_everything` database

Update from Chris: if your local server has problems authenticating to the database,
try running `ALTER USER postgres PASSWORD 'yourPassword;'`

### Configure

Create a file that contains your `dev` (local) profile configuration overrides.
The file should be created here:

```
${QUESTION_EVERYTHING_HOME}/src/main/resources/application-dev.properties
```

To set the Postgres username and password, the following properties should be
added to the file:

```properties
spring.datasource.username=POSTGRES_USERNAME
spring.datasource.password=POSTGRES_PASSWORD
```

### Run

```sh
gradlew bootRun                 # build & run the application for development
gradlew bootRun -Pprofile=prod  # build & run the application for production
gradlew build                   # build only
``` 

## Testing

To run the different types of unit tests, the following commands can be run:

```sh
gradlew test                     # server-side unit tests
gradlew test -Ptests=integration # server-side integration tests
ng test                          # client-side unit tests (run continously)
ng test --no-watch               # client-side unit tests (run once)
ng e2e                           # client-side integration tests
```
